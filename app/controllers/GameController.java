package controllers;

import java.util.*;

import highscore.Failure;
import highscore.PublishHighScoreEndpoint;
import highscore.PublishHighScoreService;
import highscore.data.GenderType;
import highscore.data.HighScoreRequestType;
import highscore.data.UserDataType;
import highscore.data.UserType;
import models.*;
import highscore.PublishHighScoreEndpoint;
import models.Category;
import models.JeopardyDAO;
import models.JeopardyGame;
import models.JeopardyUser;
import play.Logger;
import play.cache.Cache;
import play.data.DynamicForm;
import play.data.DynamicForm.Dynamic;
import play.data.Form;
import play.db.jpa.Transactional;
import play.i18n.Messages;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import twitter.TwitterClient;
import twitter.TwitterStatusMessage;
import views.html.jeopardy;
import views.html.question;
import views.html.winner;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.Endpoint;
import javax.xml.ws.WebServiceException;

@Security.Authenticated(Secured.class)
public class GameController extends Controller {
	
	protected static final int CATEGORY_LIMIT = 5;
	
	@Transactional
	public static Result index() {
		return redirect(routes.GameController.playGame());
	}
	
	@play.db.jpa.Transactional(readOnly = true)
	private static JeopardyGame createNewGame(String userName) {
		return createNewGame(JeopardyDAO.INSTANCE.findByUserName(userName));
	}
	
	@play.db.jpa.Transactional(readOnly = true)
	private static JeopardyGame createNewGame(JeopardyUser user) {
		if(user == null) // name still stored in session, but database dropped
			return null;

		Logger.info("[" + user + "] Creating a new game.");
		List<Category> allCategories = JeopardyDAO.INSTANCE.findEntities(Category.class);
		if(allCategories.size() > CATEGORY_LIMIT) {
			// select 5 categories randomly (simple)
			Collections.shuffle(allCategories);
			allCategories = allCategories.subList(0, CATEGORY_LIMIT);
		}
		Logger.info("Start game with " + allCategories.size() + " categories.");
		JeopardyGame game = new JeopardyGame(user, allCategories);
		cacheGame(game);
		return game;
	}
	
	private static void cacheGame(JeopardyGame game) {
		Cache.set(gameId(), game, 3600);
	}
	
	private static JeopardyGame cachedGame(String userName) {
		Object game = Cache.get(gameId());
		if(game instanceof JeopardyGame)
			return (JeopardyGame) game;
		return createNewGame(userName);
	}
	
	private static String gameId() {
		return "game." + uuid();
	}

	private static String uuid() {
		String uuid = session("uuid");
		if (uuid == null) {
			uuid = UUID.randomUUID().toString();
			session("uuid", uuid);
		}
		return uuid;
	}
	
	@Transactional
	public static Result newGame() {
		Logger.info("[" + request().username() + "] Start new game.");
		JeopardyGame game = createNewGame(request().username());
		return ok(jeopardy.render(game));
	}
	
	@Transactional
	public static Result playGame() {
		Logger.info("[" + request().username() + "] Play the game.");
		JeopardyGame game = cachedGame(request().username());
		if(game == null) // e.g., username still in session, but db dropped
			return redirect(routes.Authentication.login());
		if(game.isAnswerPending()) {
			Logger.info("[" + request().username() + "] Answer pending... redirect");
			return ok(question.render(game));
		} else if(game.isGameOver()) {
			Logger.info("[" + request().username() + "] Game over... redirect");
			return ok(winner.render(game, null, null, null));
		}			
		return ok(jeopardy.render(game));
	}
	
	@play.db.jpa.Transactional(readOnly = true)
	public static Result questionSelected() {
		JeopardyGame game = cachedGame(request().username());
		if(game == null || !game.isRoundStart())
			return redirect(routes.GameController.playGame());
		
		Logger.info("[" + request().username() + "] Questions selected.");		
		DynamicForm form = Form.form().bindFromRequest();
		
		String questionSelection = form.get("question_selection");
		
		if(questionSelection == null || questionSelection.equals("") || !game.isRoundStart()) {
			return badRequest(jeopardy.render(game));
		}
		
		game.chooseHumanQuestion(Long.parseLong(questionSelection));
		
		return ok(question.render(game));
	}
	
	@play.db.jpa.Transactional(readOnly = true)
	public static Result submitAnswers() {
		JeopardyGame game = cachedGame(request().username());
		if(game == null || !game.isAnswerPending())
			return redirect(routes.GameController.playGame());
		
		Logger.info("[" + request().username() + "] Answers submitted.");
		Dynamic form = Form.form().bindFromRequest().get();
		
		@SuppressWarnings("unchecked")
		Map<String,String> data = form.getData();
		List<Long> answerIds = new ArrayList<>();
		
		for(String key : data.keySet()) {
			if(key.startsWith("answers[")) {
				answerIds.add(Long.parseLong(data.get(key)));
			}
		}
		game.answerHumanQuestion(answerIds);
		if(game.isGameOver()) {
			return redirect(routes.GameController.gameOver());

		} else {
			return ok(jeopardy.render(game));
		}
	}
	
	@play.db.jpa.Transactional(readOnly = true)
	public static Result gameOver() {
		JeopardyGame game = cachedGame(request().username());
		if(game == null || !game.isGameOver())
			return redirect(routes.GameController.playGame());
		
		Logger.info("[" + request().username() + "] Game over.");

		String uuid=null;
		String info = null;
		String role = null;
		String classString = null;
		try {
			uuid = sendHighscore(game);
			TwitterClient client = new TwitterClient();
			TwitterStatusMessage message = new TwitterStatusMessage(request().username(), uuid, new Date());
			client.publishUuid(message);
			info = Messages.get("post.success",uuid);
			role="success";
			classString="success";
		} catch (WebServiceException ex) {
			info = Messages.get("post.highscoreDown");
			Logger.error(info);
			role="alert";
			classString="errors";
		} catch (Failure ex) {
			info = Messages.get("post.highscoreFail");
			Logger.error(info);
			ex.printStackTrace();
			role="alert";
			classString="errors";
		} catch (Exception e){
			info = Messages.get("post.twitterFail");
			Logger.error(info);
			e.printStackTrace();
			role="alert";
			classString="errors";
		}

		return ok(winner.render(game, info,role,classString));
	}

	private static String sendHighscore(JeopardyGame game) throws Failure{
		Logger.info("[" + request().username() + "] send Highscore");

		PublishHighScoreService service = new PublishHighScoreService();

		PublishHighScoreEndpoint endpoint = service.getPublishHighScorePort();

		HighScoreRequestType requestType = new HighScoreRequestType();
		requestType.setUserKey("3ke93-gue34-dkeu9");

		UserDataType userDataType = new UserDataType();
		userDataType.setWinner(convertPlayer(game.getWinner()));
		userDataType.setLoser(convertPlayer(game.getLoser()));

		requestType.setUserData(userDataType);

		Logger.info("[" + request().username() + "] publish HighScore...");

		String ret = endpoint.publishHighScore(requestType);
		Logger.info("[" + request().username() + "] UUID: " + ret);

		return ret;
	}

	private static UserType convertPlayer(Player p){
		UserType userType = new UserType();

		userType.setFirstName(p.getUser().getFirstName());
		userType.setLastName(p.getUser().getLastName());

		userType.setGender(GenderType.fromValue(p.getUser().getGender().toString()));

		try {
			GregorianCalendar c = new GregorianCalendar();
			Date date = p.getUser().getBirthDate();
			if(date == null) {
				Logger.warn("birth date of user is null -> send a new Date to the highscoreboard");
				date = new Date();
			}
			c.setTime(date);
			XMLGregorianCalendar xmlGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
			userType.setBirthDate(xmlGregorianCalendar);
			userType.getBirthDate().setTimezone(DatatypeConstants.FIELD_UNDEFINED);
		} catch (DatatypeConfigurationException e) {
			Logger.error("faild to convert birth date");
		}

		//Passwort muss leer bleiben. Aus Angabe!
		userType.setPassword("");
		userType.setPoints(p.getProfit());

		return userType;
	}
}
