import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import at.ac.tuwien.big.we.dbpedia.api.DBPediaService;
import at.ac.tuwien.big.we.dbpedia.api.SelectQueryBuilder;
import at.ac.tuwien.big.we.dbpedia.vocabulary.DBPedia;
import at.ac.tuwien.big.we.dbpedia.vocabulary.DBPediaOWL;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.sparql.vocabulary.FOAF;
import com.hp.hpl.jena.vocabulary.RDF;
import com.hp.hpl.jena.vocabulary.RDFS;
import models.*;
import play.Application;
import play.GlobalSettings;
import play.Logger;
import play.Play;
import play.db.jpa.JPA;
import play.db.jpa.Transactional;
import play.i18n.Messages;
import play.libs.F.Function0;
import data.JSONDataInserter;

public class Global extends GlobalSettings {
	
	@play.db.jpa.Transactional
	public static void insertJSonData() throws IOException {
		String file = Play.application().configuration().getString("questions.filePath");		
		Logger.info("Data from: " + file);
		InputStream is = Play.application().resourceAsStream(file);
		List<Category> categories = JSONDataInserter.insertData(is);
		Logger.info(categories.size() + " categories from json file '" + file + "' inserted.");
	}
	
	@play.db.jpa.Transactional
    public void onStart(Application app) {

       try {
    	   JPA.withTransaction(new Function0<Boolean>() {

			@Override
			public Boolean apply() throws Throwable {
				insertJSonData();
				Category category=new Category();
				category.setName("Spezial", "de");
				category.setName("Special", "en");

				Question question=new Question();
				question.setCategory(category);
				question.setText(Messages.get("question.soccer1"), Messages.get("lang"));
				question.setValue(10);

				generateQuestionSoccer(category, question);

				question=new Question();
				question.setCategory(category);
				question.setText(Messages.get("question.sing"), Messages.get("lang"));

				System.out.print(Locale.getDefault() + " " + question.getTextDE());
				question.setValue(20);

				generateQuestionSinger(category, question);

				question=new Question();
				question.setCategory(category);
				question.setText(Messages.get("question.science"), Messages.get("lang"));
				question.setValue(60);

				generateQuestionSience(category, question);

				question=new Question();
				question.setCategory(category);

				question.setText(Messages.get("question.movie"), Messages.get("lang"));
				question.setValue(30);

				generateQuestionMovie(category, question);

				question=new Question();
				question.setCategory(category);
				question.setText(Messages.get("question.soccer2"), Messages.get("lang"));
				question.setValue(100);

				generateQuestionSoccer2(category, question);
				return true;
			}
			   
			});
       } catch (Throwable e) {
    	   e.printStackTrace();
       }

    }

    public void onStop(Application app) {
        Logger.info("Application shutdown...");
    }

	@play.db.jpa.Transactional
	public void generateQuestionSinger(Category category, Question question)
	{
		// Check if DBpedia is available
		if(!DBPediaService.isAvailable())
			return;

		Resource band = DBPediaService.loadStatements(DBPedia.createResource("Austria"));

		String englishActorName = DBPediaService.getResourceName(band, Locale.ENGLISH);
		String germanActorName = DBPediaService.getResourceName(band, Locale.GERMAN);
		// build SPARQL-query
		SelectQueryBuilder movieQuery = DBPediaService.createQueryBuilder()
				.setLimit(4) // at most five statements
				.addWhereClause(RDF.type, DBPediaOWL.MusicalArtist)
				.addPredicateExistsClause(FOAF.name)
				.addWhereClause(DBPediaOWL.birthPlace  , band)
				.addFilterClause(RDFS.label, Locale.GERMAN)
				.addFilterClause(RDFS.label, Locale.ENGLISH);


		Model englishTeams = DBPediaService.loadStatements(movieQuery.toQueryString());

		List<String> englishTeamNames =
				DBPediaService.getResourceNames(englishTeams, Locale.ENGLISH);
		List<String> germanTeamNames =
				DBPediaService.getResourceNames(englishTeams, Locale.GERMAN);

		movieQuery.removeWhereClause(DBPediaOWL.birthPlace, band);
		movieQuery.addMinusClause(DBPediaOWL.birthPlace, band);

		Model noTeams = DBPediaService.loadStatements(movieQuery.toQueryString());

		List<String> englishnoTeams =
				DBPediaService.getResourceNames(noTeams, Locale.ENGLISH);
		List<String> germannoTeams =
				DBPediaService.getResourceNames(noTeams, Locale.GERMAN);

		List<Answer> answers=new ArrayList<>();
		int index=0;
		for(String s:englishTeamNames)
		{
			Answer a=new Answer();
			a.setCorrectAnswer(true);
			a.setQuestion(question);
			a.setText("What is " + s, "en");
			a.setText("Was ist " + germanTeamNames.get(index),"de");
			answers.add(a);
			index++;
		}
		index=0;
		for(String s:englishnoTeams)
		{
			Answer a=new Answer();
			a.setCorrectAnswer(false);
			a.setQuestion(question);
			a.setText("What is " + s, "en");
			a.setText("Was ist " + germannoTeams.get(index),"de");
			answers.add(a);
			index++;
		}

		question.setAnswers(answers);
		category.setQuestions(Arrays.asList(question));

		JeopardyDAO.INSTANCE.persist(category);
	}

	@play.db.jpa.Transactional
	public void generateQuestionSience(Category category, Question question)
	{
		// Check if DBpedia is available
		if(!DBPediaService.isAvailable())
			return;

		Resource league = DBPediaService.loadStatements(DBPedia.createResource("Austria"));

		String englishActorName = DBPediaService.getResourceName(league, Locale.ENGLISH);
		String germanActorName = DBPediaService.getResourceName(league, Locale.GERMAN);
		// build SPARQL-query
		SelectQueryBuilder movieQuery = DBPediaService.createQueryBuilder()
				.setLimit(3) // at most five statements
				.addWhereClause(RDF.type, DBPediaOWL.Scientist)
				.addPredicateExistsClause(FOAF.name)
				.addWhereClause(DBPediaOWL.birthPlace , league)
				.addFilterClause(RDFS.label, Locale.GERMAN)
				.addFilterClause(RDFS.label, Locale.ENGLISH);


		Model englishTeams = DBPediaService.loadStatements(movieQuery.toQueryString());

		List<String> englishTeamNames =
				DBPediaService.getResourceNames(englishTeams, Locale.ENGLISH);
		List<String> germanTeamNames =
				DBPediaService.getResourceNames(englishTeams, Locale.GERMAN);

		movieQuery.removeWhereClause(RDF.type, DBPediaOWL.Scientist);
		movieQuery.addMinusClause(RDF.type, DBPediaOWL.Scientist);

		Model noTeams = DBPediaService.loadStatements(movieQuery.toQueryString());

		List<String> englishnoTeams =
				DBPediaService.getResourceNames(noTeams, Locale.ENGLISH);
		List<String> germannoTeams =
				DBPediaService.getResourceNames(noTeams, Locale.GERMAN);

		List<Answer> answers=new ArrayList<>();
		int index=0;
		for(String s:englishTeamNames)
		{
			Answer a=new Answer();
			a.setCorrectAnswer(true);
			a.setQuestion(question);
			a.setText("What is " + s, "en");
			a.setText("Was ist " + germanTeamNames.get(index),"de");
			answers.add(a);
			index++;
		}
		index=0;
		for(String s:englishnoTeams)
		{
			Answer a=new Answer();
			a.setCorrectAnswer(false);
			a.setQuestion(question);
			a.setText("What is " + s, "en");
			a.setText("Was ist " + germannoTeams.get(index),"de");
			answers.add(a);
			index++;
		}
		question.setAnswers(answers);
		category.setQuestions(Arrays.asList(question));

		JeopardyDAO.INSTANCE.persist(category);
	}

	@play.db.jpa.Transactional
	public void generateQuestionMovie(Category category, Question question)
	{
		// Check if DBpedia is available
		if(!DBPediaService.isAvailable())
			return;

		Resource band = DBPediaService.loadStatements(DBPedia.createResource("Edward_Norton"));

		String englishActorName = DBPediaService.getResourceName(band, Locale.ENGLISH);
		String germanActorName = DBPediaService.getResourceName(band, Locale.GERMAN);
		// build SPARQL-query
		SelectQueryBuilder movieQuery = DBPediaService.createQueryBuilder()
				.setLimit(2) // at most five statements
				.addWhereClause(RDF.type, DBPediaOWL.Film)
				.addPredicateExistsClause(FOAF.name)
				.addWhereClause(DBPediaOWL.starring, band)
				.addFilterClause(RDFS.label, Locale.GERMAN)
				.addFilterClause(RDFS.label, Locale.ENGLISH);


		Model englishTeams = DBPediaService.loadStatements(movieQuery.toQueryString());

		List<String> englishTeamNames =
				DBPediaService.getResourceNames(englishTeams, Locale.ENGLISH);
		List<String> germanTeamNames =
				DBPediaService.getResourceNames(englishTeams, Locale.GERMAN);

		movieQuery.removeWhereClause(DBPediaOWL.starring, band);
		movieQuery.addMinusClause(DBPediaOWL.starring, band);

		Model noTeams = DBPediaService.loadStatements(movieQuery.toQueryString());

		List<String> englishnoTeams =
				DBPediaService.getResourceNames(noTeams, Locale.ENGLISH);
		List<String> germannoTeams =
				DBPediaService.getResourceNames(noTeams, Locale.GERMAN);

		List<Answer> answers=new ArrayList<>();
		int index=0;
		for(String s:englishTeamNames)
		{
			Answer a=new Answer();
			a.setCorrectAnswer(true);
			a.setQuestion(question);
			a.setText("What is " + s, "en");
			a.setText("Was ist " + germanTeamNames.get(index),"de");
			answers.add(a);
			index++;
		}
		index=0;
		for(String s:englishnoTeams)
		{
			Answer a=new Answer();
			a.setCorrectAnswer(false);
			a.setQuestion(question);
			a.setText("What is " + s, "en");
			a.setText("Was ist " + germannoTeams.get(index),"de");
			answers.add(a);
			index++;
		}

		question.setAnswers(answers);
		category.setQuestions(Arrays.asList(question));

		JeopardyDAO.INSTANCE.persist(category);
	}

	@play.db.jpa.Transactional
	public void generateQuestionSoccer2(Category category, Question question)
	{
		// Check if DBpedia is available
		if(!DBPediaService.isAvailable())
			return;

		Resource band = DBPediaService.loadStatements(DBPedia.createResource("Manchester_United_F.C."));

		String englishActorName = DBPediaService.getResourceName(band, Locale.ENGLISH);
		String germanActorName = DBPediaService.getResourceName(band, Locale.GERMAN);
		// build SPARQL-query
		SelectQueryBuilder movieQuery = DBPediaService.createQueryBuilder()
				.setLimit(2) // at most five statements
				.addWhereClause(RDF.type, DBPediaOWL.SoccerPlayer)
				.addPredicateExistsClause(FOAF.name)
				.addWhereClause(DBPediaOWL.team, band)
				.addFilterClause(RDFS.label, Locale.GERMAN)
				.addFilterClause(RDFS.label, Locale.ENGLISH);


		Model englishTeams = DBPediaService.loadStatements(movieQuery.toQueryString());

		List<String> englishTeamNames =
				DBPediaService.getResourceNames(englishTeams, Locale.ENGLISH);
		List<String> germanTeamNames =
				DBPediaService.getResourceNames(englishTeams, Locale.GERMAN);

		movieQuery.removeWhereClause(DBPediaOWL.team, band);
		movieQuery.addMinusClause(DBPediaOWL.team, band);

		Model noTeams = DBPediaService.loadStatements(movieQuery.toQueryString());

		List<String> englishnoTeams =
				DBPediaService.getResourceNames(noTeams, Locale.ENGLISH);
		List<String> germannoTeams =
				DBPediaService.getResourceNames(noTeams, Locale.GERMAN);

		List<Answer> answers=new ArrayList<>();
		int index=0;
		for(String s:englishTeamNames)
		{
			Answer a=new Answer();
			a.setCorrectAnswer(false);
			a.setQuestion(question);
			a.setText("What is " + s, "en");
			a.setText("Was ist " + germanTeamNames.get(index),"de");
			answers.add(a);
			index++;
		}
		index=0;
		for(String s:englishnoTeams)
		{
			Answer a=new Answer();
			a.setCorrectAnswer(true);
			a.setQuestion(question);
			a.setText("What is " + s, "en");
			a.setText("Was ist " + germannoTeams.get(index),"de");
			answers.add(a);
			index++;
		}

		question.setAnswers(answers);
		category.setQuestions(Arrays.asList(question));

		JeopardyDAO.INSTANCE.persist(category);
	}

	@play.db.jpa.Transactional
	public void generateQuestionSoccer(Category category, Question question)
	{
		// Check if DBpedia is available
		if(!DBPediaService.isAvailable())
			return;

		Resource league = DBPediaService.loadStatements(DBPedia.createResource("Premier_League"));

		String englishActorName = DBPediaService.getResourceName(league, Locale.ENGLISH);
		String germanActorName = DBPediaService.getResourceName(league, Locale.GERMAN);
		// build SPARQL-query
		SelectQueryBuilder movieQuery = DBPediaService.createQueryBuilder()
				.setLimit(3) // at most five statements
				.addWhereClause(RDF.type, DBPediaOWL.SoccerClub)
				.addPredicateExistsClause(FOAF.name)
				.addWhereClause(DBPediaOWL.league, league)
				.addFilterClause(RDFS.label, Locale.GERMAN)
				.addFilterClause(RDFS.label, Locale.ENGLISH);


		Model englishTeams = DBPediaService.loadStatements(movieQuery.toQueryString());

		List<String> englishTeamNames =
				DBPediaService.getResourceNames(englishTeams, Locale.ENGLISH);
		List<String> germanTeamNames =
				DBPediaService.getResourceNames(englishTeams, Locale.GERMAN);

		movieQuery.removeWhereClause(DBPediaOWL.league, league);
		movieQuery.addMinusClause(DBPediaOWL.league, league);

		Model noTeams = DBPediaService.loadStatements(movieQuery.toQueryString());

		List<String> englishnoTeams =
				DBPediaService.getResourceNames(noTeams, Locale.ENGLISH);
		List<String> germannoTeams =
				DBPediaService.getResourceNames(noTeams, Locale.GERMAN);

		List<Answer> answers=new ArrayList<>();
		int index=0;
		for(String s:englishTeamNames)
		{
			Answer a=new Answer();
			a.setCorrectAnswer(true);
			a.setQuestion(question);
			a.setText("What is " + s, "en");
			a.setText("Was ist " + germanTeamNames.get(index),"de");
			answers.add(a);
			index++;
		}
		index=0;
		for(String s:englishnoTeams)
		{
			Answer a=new Answer();
			a.setCorrectAnswer(false);
			a.setQuestion(question);
			a.setText("What is " + s, "en");
			a.setText("Was ist " + germannoTeams.get(index),"de");
			answers.add(a);
			index++;
		}

		question.setAnswers(answers);
		category.setQuestions(Arrays.asList(question));

		JeopardyDAO.INSTANCE.persist(category);
	}

	@play.db.jpa.Transactional
	public void generateQuestion(Category category, Question question)
	{
		// Check if DBpedia is available
		if(!DBPediaService.isAvailable())
			return;
		// Resource Tim Burton is available at http://dbpedia.org/resource/Tim_Burton
		// Load all statements as we need to get the name later
		Resource director = DBPediaService.loadStatements(DBPedia.createResource("Tim_Burton"));
		// Resource Johnny Depp is available at http://dbpedia.org/resource/Johnny_Depp
		// Load all statements as we need to get the name later
		Resource actor = DBPediaService.loadStatements(DBPedia.createResource("Johnny_Depp"));
		// retrieve english and german names, might be used for question text
		String englishDirectorName = DBPediaService.getResourceName(director, Locale.ENGLISH);
		String germanDirectorName = DBPediaService.getResourceName(director, Locale.GERMAN);
		String englishActorName = DBPediaService.getResourceName(actor, Locale.ENGLISH);
		String germanActorName = DBPediaService.getResourceName(actor, Locale.GERMAN);
		// build SPARQL-query
		SelectQueryBuilder movieQuery = DBPediaService.createQueryBuilder()
				.setLimit(5) // at most five statements
				.addWhereClause(RDF.type, DBPediaOWL.Film)
				.addPredicateExistsClause(FOAF.name)
				.addWhereClause(DBPediaOWL.director, director)
				.addFilterClause(RDFS.label, Locale.GERMAN)
				.addFilterClause(RDFS.label, Locale.ENGLISH);
		// retrieve data from dbpedia
		Model timBurtonMovies = DBPediaService.loadStatements(movieQuery.toQueryString());
		// get english and german movie names, e.g., for right choices
		List<String> englishTimBurtonMovieNames =
				DBPediaService.getResourceNames(timBurtonMovies, Locale.ENGLISH);
		List<String> germanTimBurtonMovieNames =
				DBPediaService.getResourceNames(timBurtonMovies, Locale.GERMAN);
		// alter query to get movies without tim burton
		movieQuery.removeWhereClause(DBPediaOWL.director, director);
		movieQuery.addMinusClause(DBPediaOWL.director, director);
		// retrieve data from dbpedia
		Model noTimBurtonMovies = DBPediaService.loadStatements(movieQuery.toQueryString());
		// get english and german movie names, e.g., for wrong choices
		List<String> englishNoTimBurtonMovieNames =
				DBPediaService.getResourceNames(noTimBurtonMovies, Locale.ENGLISH);
		List<String> germanNoTimBurtonMovieNames =
				DBPediaService.getResourceNames(noTimBurtonMovies, Locale.GERMAN);



		List<Answer> answers=new ArrayList<>();
		int index=0;
		for(String s:englishTimBurtonMovieNames)
		{
			Answer a=new Answer();
			a.setCorrectAnswer(true);
			a.setQuestion(question);
			a.setText("What is " + s, "en");
			a.setText("Was ist " + germanTimBurtonMovieNames.get(index),"de");
			answers.add(a);
			index++;
		}
		index=0;
		for(String s:englishNoTimBurtonMovieNames)
		{
			Answer a=new Answer();
			a.setCorrectAnswer(false);
			a.setQuestion(question);
			a.setText("What is " + s, "en");
			a.setText("Was ist " + germanNoTimBurtonMovieNames.get(index),"de");
			answers.add(a);
			index++;
		}
		question.setAnswers(answers);
		category.setQuestions(Arrays.asList(question));

		JeopardyDAO.INSTANCE.persist(category);
	}
}